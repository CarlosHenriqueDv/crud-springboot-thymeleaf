#FROM maven:3.6-jdk-8-alpine AS builder
#
#WORKDIR /build
#COPY . /build
#
#RUN mvn clean package


FROM adoptopenjdk/openjdk8:ubi
ARG JAR_FILE=target/=*.jar
VOLUME /tmp
ADD target/demo-mvc-0.0.1-SNAPSHOT.jar /app.jar
#COPY --from=builder /build/target/*.jar /app.jar
#ENV JAVA_OPTS=""
#COPY ${JAR_FILE} /app.jar
#COPY /crud-springboot-thymeleaf-gitlab/target/demo-mvc-0.0.1-SNAPSHOT.jar crud-springboot-thymeleaf-gitlab/docker/teste.jar

RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]

EXPOSE 8080
#
#CMD java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -jar /demo-mvc-0.0.1-SNAPSHOT.jar